#!/usr/bin/env bash

# Stop on errors
set -e

echo "Executing test.sh"

# Initialize submodules
git submodule update --init --recursive

# Create a brand new test project.
mkdir -p ../test_project
pushd ../test_project
git init
touch README.md
../bootstrap/bootstrap.sh
scripts/build.sh

source MANIFEST
final=$(echo "${BUILDS[-1]}" | awk -F'#' '{print $2}')
echo "Final build tag = ${final}"
${docker_cmd} run -e BATCH=1 ${final}
popd
