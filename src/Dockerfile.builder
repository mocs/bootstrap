# This Dockerfile shows how to do a multi-stage build by the mocs
# way of working.

################################################################################
# Install fortune and cowsay just to show how it's done
################################################################################
ARG arch
ARG ver
FROM registry.gitlab.com/mocs/mocsci/centos-minimalist-image/centos-yum:${ver}_${arch}
ARG arch

# Set the target folder
ARG target_root=/target_image/rootfs

# Allow epel repo
RUN set -eux; yum -y \
  --setopt=tsflags='nodocs' \
  --releasever=7 \
  --setopt=override_install_langs=en_US.utf8 install \
  epel-release

# Install packages
RUN set -eux; yum -y \
  --setopt=tsflags='nodocs' \
  --releasever=7 \
  --setopt=override_install_langs=en_US.utf8 install \
  fortune-mod \
  cowsay

# Install copy libs script
COPY ./scripts/bin/*.sh /usr/bin/
COPY ./scripts/common/*.sh /usr/bin/

# Copy fortune
RUN mkdir -p ${target_root}/bin \
  && cp /usr/bin/fortune ${target_root}/bin/
RUN copy_dynamic_libs.sh /usr/bin/fortune ${target_root}
RUN mkdir -p ${target_root}/usr/share/games/fortune \
  && cp /usr/share/games/fortune/* ${target_root}/usr/share/games/fortune/

# Copy cowsay
RUN mkdir -p ${target_root}/bin \
  && cp /usr/bin/cowsay ${target_root}/bin/
RUN mkdir -p ${target_root}/usr/share \
  && cp -r /usr/share/cowsay ${target_root}/usr/share/
# which requires perl!
RUN mkdir -p ${target_root}/bin \
  && cp /usr/bin/perl ${target_root}/bin/
RUN copy_dynamic_libs.sh /usr/bin/perl ${target_root}
RUN mkdir -p ${target_root}/usr/share/perl5 \
  && cp -r /usr/share/perl5/* ${target_root}/usr/share/perl5/
RUN mkdir -p ${target_root}/usr/lib64/perl5 && \
    mkdir -p ${target_root}/usr/lib/perl5 && \
    ln -s ${target_root}/usr/lib64 ${target_root}/lib64 && \
    ln -s ${target_root}/usr/lib ${target_root}/lib && \
    if [[ -d /usr/lib64/perl5 ]]; then \
      cp -r /usr/lib64/perl5/* ${target_root}/usr/lib64/perl5/ ; \
    else \
      cp -r /usr/lib/perl5/* ${target_root}/usr/lib/perl5/ ; \
    fi

# Show user what got built!
# RUN tree ${target_root}
