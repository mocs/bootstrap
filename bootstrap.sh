#!/usr/bin/env bash

# Script to generate a new project according to this template.
# Create a new repository first. Should only contain a README.md!
# Execute this script from that folder!

# Always die on error
set -e

# Get the folder name of _this_ script
SRCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Get the folder of the executing instance
TARGET=$(pwd)

# Get common stuff
source ${SRCDIR}/scripts/common/shell.sh

# Sanity checks
printf "${MAGENTA}"
message "Executing sanity checks!"
printf "${NC}"
printf "${cyan}"
printf "     Checking that we do not execute in self..."
if [[ "${TARGET}" == "${SRCDIR}" ]]; then
  printf "\r"
  _exit_cmd "This script cannot be executed in this folder." 1
else
  printf "\r${GREEN}OK\n${NC}"
fi
printf "${cyan}"
printf "     Checking that we have a git repo..."
if ! [[ -d .git ]]; then
  printf "\r"
  _exit_cmd "This script should only be executed in a git repository." 1
else
  printf "\r${GREEN}OK\n${NC}"
fi
printf "${cyan}"
printf "     Checking that we have a README.md..."
if ! [[ -f "README.md" ]]; then
  printf "\r"
  _exit_cmd "There is no README.md file." 1
else
  printf "\r${GREEN}OK\n${NC}"
fi
printf "${cyan}"
printf "     Checking that we ${CYAN}only${cyan} have a README.md..."
count=$(ls | wc -l)
if [[ $count -ne 1 ]]; then
  printf "\r"
  _exit_cmd "There are more files than the README.md in this folder." 1
else
  printf "\r${GREEN}OK\n${NC}"
fi
printf "${cyan}"
printf "     Checking that we ${CYAN}do not${cyan} have a .gitignore..."
count=$(ls | wc -l)
if [[ -f ".gitignore" ]]; then
  printf "\r"
  _exit_cmd "There is a .gitignore file in this folder." 1
else
  printf "\r${GREEN}OK\n${NC}"
fi

# Initialize repository
printf "${MAGENTA}"
message "Initializing repository"
printf "${NC}"

# Copy files
printf "${cyan}"
printf "     Copying files..."
printf "${NC}"
cp ${SRCDIR}/LICENSE ${TARGET}
cp ${SRCDIR}/MANIFEST ${TARGET}
cp ${SRCDIR}/DEPENDENCIES ${TARGET}
cp ${SRCDIR}/update.sh ${TARGET}
cp -r ${SRCDIR}/src ${TARGET}
cp ${SRCDIR}/.gitignore ${TARGET}
printf "\r${GREEN}OK\n${NC}"

# Add the standard submodules
printf "${cyan}"
printf "     Adding submodules..."
printf "${NC}"
git submodule --quiet add https://gitlab.com/mocs/scripts.git
git submodule --quiet update --init --recursive
printf "\r${GREEN}OK\n${NC}"

# All done!
printf "${MAGENTA}"
message "All done!"
printf "${NC}"
